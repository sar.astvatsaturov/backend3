<!DOCTYPE html>
<html>
	<head>
        <link rel="shortcut icon" href="logo.png" type="image/png">
 		<title>backend3</title>
 		<style>
            body {
                background-color: black;
                font-weight: 700;
                font-size: x-large;
            }
            form {
                background-color: white;
                border-radius: 30px;
                text-align: center;
                padding: 100px;
                width: 50%;
                height: 90%;
                margin: auto;
            }
            label {
                font-size: medium;
            }
            input, select, textarea {
                margin-bottom: 20px;
            }
        </style>
	</head>
	<body>
		<form name="form" method="POST">
            Имя:
            <label>
                <br>
                <input type="text" name="field-name" placeholder="Введите ваше имя">
            </label>
            <br>
            Email:
            <label>
               
                <br>
                <input type="email" name="field-email" placeholder="test@example.com">
            </label>
            <br>
            Дата рождения:
            <label>
                <br>
                <input type="date" name="field-date" value="1999-01-01">
            </label><br> Пол:
            <br>
            
 
            <label class="radio"><input type="radio" name="radio-sex" value="М">
    Мужской</label>
            <label class="radio"><input type="radio" name="radio-sex" value="Ж">
    Женский</label><br>
            <br> Количество конечностей:
            <br>
            <label>
                <input type="radio" name="radio-limb" value="0">
                0
            </label>
            <label>
                <input type="radio" name="radio-limb" value="1">
                1
            </label>
            <label>
                <input type="radio" name="radio-limb" value="2">
                2
            </label>
            <label>
                <input type="radio" name="radio-limb" value="3">
                3
            </label>
            <label>
                <input type="radio" name="radio-limb" value="4">
                4
            </label>
            <br>
            Сверхспособности:
            <label>
                <br>
                <select name="superpower[]" multiple="multiple">
                    <option value="Бессмертие">Бессмертие</option>
                    <option value="Ливитация" >Левитация</option>
                    <option value="Невидимость">Нивидимость</option>
                </select>
            </label>
            <br>
            Биография:
            <label>
                <br>
                <textarea name="BIO" placeholder="Расскажите о себе"></textarea>
            </label>
            <br>
            <label>
                <input name="accept" type="checkbox" value=1>
                С контрактом ознакомлен
            </label>
            <br>
            <input type="submit" value="Отправить">
        </form>
	</body>
</html>
